package beans;

import java.io.IOException;
import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;

import com.liferay.faces.util.logging.Logger;
import com.liferay.faces.util.logging.LoggerFactory;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.Role;
import com.liferay.portal.kernel.model.RoleConstants;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.UserLocalService;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;

import ideaService.model.Category;
import ideaService.model.Ideas;
import ideaService.service.CategoryLocalService;
import ideaService.service.IdeasLocalService;
import ideasService.service.enums.ReviewStatus;
import projectService.service.ProjectLocalService;
import servicetrackers.CategoryLocalServiceTracker;
import servicetrackers.IdeaLocalServiceTracker;
import servicetrackers.ProjectLocalServiceTracker;
import servicetrackers.UserLocalServiceTracker;

import ideaService.service.IdeasLocalServiceUtil;
import projectService.service.ProjectLocalServiceUtil;

@ManagedBean(name="adminIdeasBean")
@SessionScoped
public class AdminIdeasBean implements Serializable {

	private static final long serialVersionUID = 8284468420268620239L;

	private IdeaLocalServiceTracker ideasLocalServiceTracker;
	private UserLocalServiceTracker userLocalServiceTracker;
	private ProjectLocalServiceTracker projectLocalServiceTracker;
	private CategoryLocalServiceTracker categoryLocalServiceTracker;


	private List<Ideas> ideas;
	private List<Category> categories;

	private static Logger logger;

	/**
     * This method is called everytime the page is refreshed.
     */
    public void onPageLoad(){
			updateIdeas();
    }

    @PostConstruct
    public void postConstruct() {
        Bundle bundle = FrameworkUtil.getBundle(this.getClass());
        BundleContext bundleContext = bundle.getBundleContext();
        ideasLocalServiceTracker = new IdeaLocalServiceTracker(bundleContext);
        userLocalServiceTracker = new UserLocalServiceTracker(bundleContext);
        projectLocalServiceTracker = new ProjectLocalServiceTracker(bundleContext);
        categoryLocalServiceTracker = new CategoryLocalServiceTracker(bundleContext);
        categoryLocalServiceTracker.open();
        projectLocalServiceTracker.open();
        ideasLocalServiceTracker.open();
        userLocalServiceTracker.open();


		logger = LoggerFactory.getLogger(this.getClass().getName());
		logger.info(this.getClass().getName() + " initialized successfully");
    }

    @PreDestroy
    public void preDestroy() {
    	ideasLocalServiceTracker.close();
    	userLocalServiceTracker.close();
    	projectLocalServiceTracker.close();
    	categoryLocalServiceTracker.close();
    }


    /**
     * updates the current set of ideas.
     */
    public void updateIdeas(){
    	ThemeDisplay themeDisplay = (ThemeDisplay) FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get(WebKeys.THEME_DISPLAY);
    	IdeasLocalService ideasLocalService = ideasLocalServiceTracker.getService();
    	CategoryLocalService categoryLocalService = categoryLocalServiceTracker.getService();
    	ProjectLocalService pservice = projectLocalServiceTracker.getService();
    	long projectId = 0L;
    	try{
    		projectId = pservice.getProjectByLayoutIdRef(themeDisplay.getLayout().getPrimaryKey()).getPrimaryKey();
    	}catch(Exception e){
    		logger.info("Could not find project id. Returning all ideas.");
    	}

		User user;
		try {
			user = getCurrentUser(FacesContext.getCurrentInstance().getExternalContext().getRemoteUser());

			if(this.isAdmin() || this.isPortletContentReviewer()){
				setIdeas(ideasLocalService.getIdeasByUserRoleProjectId(RoleConstants.ADMINISTRATOR, user.getUserId(), projectId));
			}else {
				for(Role role : user.getRoles()){
						if(role.getName().equals(RoleConstants.USER)){
							setIdeas(ideasLocalService.getIdeasByUserRoleProjectId(RoleConstants.USER, user.getUserId(), projectId));
							break;
						}

					}
			}
		} catch (NumberFormatException | PortalException e) {
			//case guest user
				setIdeas(ideasLocalService.getIdeasByUserRoleProjectId(RoleConstants.GUEST, -1, projectId));
		}
    	setCategories(categoryLocalService.getAllCategoriesByProjectId(projectId));
    	logger.info("Ideas model has been updated");
    }

    public List<String> getAllCategoryTitles(){
    	List<String> result =  new ArrayList<String>();
    	for(Category c : categories){
    		result.add(c.getCategoryTitle());
    	}

    	return result;
    }

    public String getCategoryTitleById(long categoryId){
    	for(Category c : categories){
    		if(c.getPrimaryKey() == categoryId){
    			return c.getCategoryTitle();
    		}
    	}
    	return "Keine";
    }

    /**
     * notifies the appropriate model listeners to delete the idea
     * @param id ideas id
     */
	public void deleteIdea(long id){
		IdeasLocalService ideasLocalService = ideasLocalServiceTracker.getService();
		ideasLocalService.deleteIdeasAndLayoutOnCascade(id);
		logger.info("Ideas with primary key " + id + " has been deleted from the db" );
	}

	public void likeIdea(long id) throws NumberFormatException, PortalException{
		IdeasLocalService ideasLocalService = ideasLocalServiceTracker.getService();
		User user = getCurrentUser(FacesContext.getCurrentInstance().getExternalContext().getRemoteUser());
		boolean res = ideasLocalService.addUserToRating(user , id);
		if(!res){
			ideasLocalService.removeUserFromRating(user, id);
		}
		logger.info(user.getScreenName() + " has liked idea with primary key " +id);
	}

	public boolean showDeleteButton(long ideasId){
		IdeasLocalService ideasLocalService = ideasLocalServiceTracker.getService();

		try {
			Ideas i = ideasLocalService.getIdeas(ideasId);
			User user = getCurrentUser(FacesContext.getCurrentInstance().getExternalContext().getRemoteUser());
			
			if(isAdmin()) {
				return true;
			}			

			if((i.getUserId() == user.getUserId()) && (i.getReviewStatus().equals(ReviewStatus.SAVED.getReviewStatusDescription())) ){
				return true;
			}

		} catch (Exception e) {
			logger.error("Could not get Idea with primary key " + ideasId, e);
			return false;
		}

		return false;
	}

	public boolean isShowAcceptAndDecline(long ideasId){
		IdeasLocalService ideasLocalService = ideasLocalServiceTracker.getService();
		try {
			if(ideasLocalService.getIdeas(ideasId).getReviewStatus().equals(ReviewStatus.ACCEPTED.getReviewStatusDescription()) ||
					ideasLocalService.getIdeas(ideasId).getReviewStatus().equals(ReviewStatus.REJECTED.getReviewStatusDescription()) ||
					ideasLocalService.getIdeas(ideasId).getReviewStatus().equals(ReviewStatus.SAVED.getReviewStatusDescription())){
				return false;
			}
			else if(isAdmin() || isPortletContentReviewer()){
				return true;
			}
		} catch (PortalException e) {
			logger.error("Could not get status of Idea with primary key " + ideasId, e);
		}
		return false;
	}

	public boolean isShowStatusColumn(long ideasId){
		if(isAdmin() || isPortletContentReviewer()){
			return true;
		}
		return false;
	}

	public int getRatingCount(long id){
		IdeasLocalService ideasLocalService = ideasLocalServiceTracker.getService();
		return ideasLocalService.getIdeasRatingCount(id);
	}

	public boolean isLoggedIn(){
		ThemeDisplay themeDisplay = (ThemeDisplay) FacesContext.getCurrentInstance().getExternalContext().getRequestMap().get(WebKeys.THEME_DISPLAY);
		return themeDisplay.isSignedIn();
	}

	public boolean isPortletContentReviewer(){
		try {
			User user = getCurrentUser(FacesContext.getCurrentInstance().getExternalContext().getRemoteUser());
			for(Role c : user.getRoles()){
				if(c.getName().equals(RoleConstants.PORTAL_CONTENT_REVIEWER)){
					//is pcr
					return true;
				}
			}
			//Not pcr
			return false;
		} catch (NumberFormatException | PortalException e) {
			//Not logged in
			return false;
		}
	}

	public boolean isAdmin(){
		try {
			User user = getCurrentUser(FacesContext.getCurrentInstance().getExternalContext().getRemoteUser());
			for(Role c : user.getRoles()){
				if(c.getName().equals(RoleConstants.ADMINISTRATOR)){
					//is admin
					return true;
				}
			}
			//Not admin
			return false;
		} catch (NumberFormatException | PortalException e) {
			//Not logged in
			return false;
		}
	}

	public boolean hasEditPermission(long ideasId){
		IdeasLocalService ideasLocalService = ideasLocalServiceTracker.getService();
		Ideas i = ideasLocalService.fetchIdeas(ideasId);
		if(isAdmin())
			return true;
		
		if(isOwner(ideasId) && i.getReviewStatus().equals(ReviewStatus.SAVED.getReviewStatusDescription())){
			return true;
		}
		return false;
	}
	
	public boolean hasUpdateIdeaSubmittingDatePermission(long ideasId){
		if(isAdmin()) {
			return true;
		}
		return false;
	}
	
	public void extendIdea(long ideasId) {
		try {
			IdeasLocalService ideasLocalService = ideasLocalServiceTracker.getService();
		    Ideas idea = ideasLocalService.fetchIdeas(ideasId);
			
			// get current date			 
			Date currentDate = new Date();
			
			//set createDateOfIdea to Current Date
			idea.setCreateDate(currentDate);
			
			//set Rating to Null
			idea.setRating(null);
			ideasLocalService.persistIdeasAndPerformTypeChecks(idea);
		} 
		
		catch (Exception e) {
			e.printStackTrace();
		}
	}

    public boolean isOwner(long ideasId){
    	if(isAdmin()){
    		return true;
    	}else{
    	try {
			User currentUser = getCurrentUser(FacesContext.getCurrentInstance().getExternalContext().getRemoteUser());
	    	for(Ideas i : ideas){
	    		if(currentUser.getUserId() == i.getUserId()){
	    			return true;
	    		}
	    	}
		} catch (NumberFormatException | PortalException e) {
			return false;
		}
    	return false;
    	}
    }

	private User getCurrentUser(String id) throws NumberFormatException, PortalException{
		UserLocalService userLocalService = userLocalServiceTracker.getService();
		return userLocalService.getUserById(Long.parseLong(id));
	}



    /**
     * loads an idea from the db and fills the model.
     * @param ideasId
     */
	public void loadIdea(long ideasId){
		//Redirect
		FacesContext context = FacesContext.getCurrentInstance();
		ExternalContext externalContext = context.getExternalContext();
			try {
				externalContext.redirect("/web/guest/projekt-einreichen?update=" + ideasId);
			} catch (IOException e) {

			}
	}
	
	public String formatDateToGerman(Date ideaCreateDate) {
		Locale locale = new Locale("de", "DE");
		String pattern = "E MMM dd HH:mm:ss z yyyy";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern, locale);
		simpleDateFormat.setTimeZone(TimeZone.getTimeZone("GMT+1"));
		return simpleDateFormat.format(ideaCreateDate);
	}


	/**
	 * @return the ideas
	 */
	public List<Ideas> getIdeas() {
		return ideas;
	}

	/**
	 * @param ideas the ideas to set
	 */
	public void setIdeas(List<Ideas> ideas) {
		this.ideas = ideas;
	}

	/**
	 * @return the categories
	 */
	public List<Category> getCategories() {
		return categories;
	}

	/**
	 * @param categories the categories to set
	 */
	public void setCategories(List<Category> categories) {
		this.categories = categories;
	}

}
