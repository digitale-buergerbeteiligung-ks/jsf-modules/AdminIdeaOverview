package beans;

import java.util.Date;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.faces.bean.ManagedBean;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.primefaces.context.RequestContext;

import com.liferay.faces.util.logging.Logger;
import com.liferay.faces.util.logging.LoggerFactory;
import com.liferay.mail.kernel.model.MailMessage;
import com.liferay.mail.kernel.service.MailService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.UserLocalService;

import ideaService.model.Ideas;
import ideaService.model.Review;
import ideaService.service.IdeasLocalService;
import ideaService.service.ReviewLocalService;
import ideasService.service.enums.ReviewStatus;
import servicetrackers.IdeaLocalServiceTracker;
import servicetrackers.IdeaReviewLocalServiceTracker;
import servicetrackers.MailLocalServiceTracker;
import servicetrackers.UserLocalServiceTracker;

@ManagedBean(name="feedbackBean", eager = true)
public class FeedbackBean {

    private long currentIdeasId;
    private boolean accepted;
    private String feedbackText;

    private  MailLocalServiceTracker mailLocalSerivceTracker;
    private IdeaLocalServiceTracker ideasLocalServiceTracker;
    private UserLocalServiceTracker userLocalServiceTracker;
    private IdeaReviewLocalServiceTracker ideaReviewSerivceTracker;

    private static Logger logger;

    @PostConstruct
    public void postConstruct() {
        Bundle bundle = FrameworkUtil.getBundle(this.getClass());
        BundleContext bundleContext = bundle.getBundleContext();
        mailLocalSerivceTracker = new MailLocalServiceTracker(bundleContext);
        ideasLocalServiceTracker = new IdeaLocalServiceTracker(bundleContext);
        userLocalServiceTracker = new UserLocalServiceTracker(bundleContext);
        ideaReviewSerivceTracker = new IdeaReviewLocalServiceTracker(bundleContext);
        mailLocalSerivceTracker.open();
        ideasLocalServiceTracker.open();
        userLocalServiceTracker.open();
        ideaReviewSerivceTracker.open();

		logger = LoggerFactory.getLogger(this.getClass().getName());
		logger.info("FeedbackBean initialized successfully");
    }

    @PreDestroy
    public void preDestroy() {
        ideasLocalServiceTracker.close();
        userLocalServiceTracker.close();
        mailLocalSerivceTracker.close();
        ideaReviewSerivceTracker.close();
    }


    public void rejectIdea(long ideasId){
        this.currentIdeasId = ideasId;
        this.accepted = false;
        try {
			autogenerateNegativeFeedbackText();
		} catch (PortalException e) {
			 logger.error("Could not get user for creating rejection email text",e);
		}
    }

    public void releaseIdea(long ideasId){
        this.currentIdeasId = ideasId;
        this.accepted = true;
        try {
			autogeneratePositiveFeedbackText();
		} catch (PortalException e) {
			logger.error("Could not get user for creating acception email text",e);
		}
    }
	
	 public void releaseIdeawithdelay(long ideasId){
        this.currentIdeasId = ideasId;
        this.accepted = true;
        try {
			autogeneratePositiveFeedbackwithdelayText();
		} catch (PortalException e) {
			logger.error("Could not get user for creating acception email text",e);
		}
    }

   private void autogenerateNegativeFeedbackText() throws PortalException{
        IdeasLocalService ideasLocalService = ideasLocalServiceTracker.getService();
        UserLocalService userSerivce = userLocalServiceTracker.getService();

        Ideas idea = ideasLocalService.getIdeas(this.currentIdeasId);
        User u = userSerivce.getUser(idea.getUserId());


        setFeedbackText("Liebe(r) " + u.getScreenName() + ",\n\n"
                + "leider verstößt das eingereichte Projekt gegen unsere Nutzungsbedingung und wurde deshalb abgelehnt. \n \n"
                + "Wir bitten um Ihr Verständnis \n"
				+ "Viele Grüße \n"
        		+ "Weck den Herkules in dir");
        RequestContext.getCurrentInstance().update("feedbackModalForm:feedbackTextInput");
    }

   private void autogeneratePositiveFeedbackText() throws PortalException{
       IdeasLocalService ideasLocalService = ideasLocalServiceTracker.getService();
       UserLocalService userSerivce = userLocalServiceTracker.getService();

       User u = userSerivce.getUser(ideasLocalService.getIdeas(this.currentIdeasId).getUserId());
       Ideas idea = ideasLocalService.getIdeas(this.currentIdeasId);
       setFeedbackText("Liebe(r) " + u.getScreenName() + ",\n\n"
               + "Ihr Projekt wurde angenommen: https://www.weckdenherkulesindir.de/projekt-einreichen/" + idea.getPageUrl() + "\n \n" 
			   + "Nun können andere Benutzer darüber abstimmen. \n \n"
               + "Immer am ersten Werktag der Monate Februar, April, Juni, August, Oktober und Dezember geben wir bekannt, welches das Projekt mit den meisten Stimmen ist. Das Projekt, das gewonnen hat, besucht Oberbürgermeister Christian Geselle persönlich. \n \n"
               + "Viele Grüße \n"
               + "Weck den Herkules in dir");

       RequestContext.getCurrentInstance().update("feedbackModalForm:feedbackTextInput");
   }
   
    private void autogeneratePositiveFeedbackwithdelayText() throws PortalException{
       IdeasLocalService ideasLocalService = ideasLocalServiceTracker.getService();
       UserLocalService userSerivce = userLocalServiceTracker.getService();

       User u = userSerivce.getUser(ideasLocalService.getIdeas(this.currentIdeasId).getUserId());
       Ideas idea = ideasLocalService.getIdeas(this.currentIdeasId);
       setFeedbackText("Liebe(r) " + u.getScreenName() + ",\n\n"
               + "Ihr Projekt wurde angenommen: https://www.weckdenherkulesindir.de/projekt-einreichen/" + idea.getPageUrl() + "\n \n" 
			   + "Es wird jedoch erst mit Beginn des neuen Zeitabschnitts freigeschaltet, damit andere Benutzer über Ihr Projekt abstimmen können. \n \n"
               + "Immer am ersten Werktag der Monate Februar, April, Juni, August, Oktober und Dezember geben wir bekannt, welches das Projekt mit den meisten Stimmen ist. Das Projekt, das gewonnen hat, besucht Oberbürgermeister Christian Geselle persönlich. \n \n"
               + "Viele Grüße \n"
               + "Weck den Herkules in dir");

       RequestContext.getCurrentInstance().update("feedbackModalForm:feedbackTextInput");
       idea.setCreateDate(new Date());
       ideasLocalService.persistIdeasAndPerformTypeChecks(idea);
   }

    public void sendFeedback() throws PortalException{
        IdeasLocalService ideasLocalService = ideasLocalServiceTracker.getService();
        UserLocalService userSerivce = userLocalServiceTracker.getService();
        ReviewLocalService reviewService = ideaReviewSerivceTracker.getService();
        User u = userSerivce.getUser(ideasLocalService.getIdeas(currentIdeasId).getUserId());

        if(!accepted){
            ideasLocalService.setIdeaReviewStatus(currentIdeasId, ReviewStatus.REJECTED);
            logger.info("rejection email sent to user" + u.getScreenName());
        }
        else{
        	ideasLocalService.setIdeaReviewStatus(currentIdeasId, ReviewStatus.ACCEPTED);
        	 logger.info("acception email sent to user" + u.getScreenName());
        }

        Review review = reviewService.createReviewWithAutomatedDbId(accepted, feedbackText, currentIdeasId, u.getPrimaryKey());
        reviewService.persistReviewAndPerformTypeChecks(review);

        sendMail(u,feedbackText,ideasLocalService.getIdeas(this.currentIdeasId).getTitle());
        RequestContext.getCurrentInstance().update("ideaoverviewform");
    }


    public void sendMail(User u, String textBody, String title){
        MailService mailService = mailLocalSerivceTracker.getService();
        try {
            InternetAddress from = new InternetAddress("info@weckdenherkulesindir.de");
            InternetAddress to = new InternetAddress(u.getEmailAddress());
            MailMessage mailMessage = new MailMessage();
            mailMessage.setTo(to);
            mailMessage.setFrom(from);
            mailMessage.setSubject("Rückmeldung zu Ihrem Projekt (" + title + ")");
            mailMessage.setBody(textBody);
            //mailMessage.setHTMLFormat(true);
            mailService.sendEmail(mailMessage);
        } catch (AddressException e) {
        	logger.error("Could not send email to User "+ u.getScreenName(),e);
        }

    }

    @Deprecated
    public void sendMailWithPlainText() {

        MailService mailService = mailLocalSerivceTracker.getService();

        InternetAddress fromAddress = null;
        InternetAddress toAddress = null;

        try {
                fromAddress = new InternetAddress("info@weckdenherkulesindir.de");
                toAddress = new InternetAddress("info@weckdenherkulesindir.de");
                MailMessage mailMessage = new MailMessage();
                mailMessage.setTo(toAddress);
                mailMessage.setFrom(fromAddress);
                mailMessage.setSubject("Test email with HTML text");
                mailMessage.setBody("<h3><font color = RED>Diese E-Mail wurde versendet von Weck den Herkules in dir</font></h3>");
                mailMessage.setHTMLFormat(true);
                mailService.sendEmail(mailMessage);
                System.out.println("Send mail with HTML text");
        } catch (AddressException e) {
                e.printStackTrace();
        }
        }

    /**
     * @return the feedbackText
     */
    public String getFeedbackText() {
        return feedbackText;
    }

    /**
     * @param feedbackText the feedbackText to set
     */
    public void setFeedbackText(String feedbackText) {
        this.feedbackText = feedbackText;
    }

    /**
     * @return the currentIdeasId
     */
    public long getCurrentIdeasId() {
        return currentIdeasId;
    }

    /**
     * @param currentIdeasId the currentIdeasId to set
     */
    public void setCurrentIdeasId(long currentIdeasId) {
        this.currentIdeasId = currentIdeasId;
    }


	/**
	 * @return the accepted
	 */
	public boolean getAccepted() {
		return accepted;
	}

	/**
	 * @param accepted the accepted to set
	 */
	public void setAccepted(boolean accepted) {
		this.accepted = accepted;
	}

}
