package servicetrackers;

import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;

import ideaService.service.CategoryLocalService;

public class CategoryLocalServiceTracker extends ServiceTracker<CategoryLocalService, CategoryLocalService> {

    public CategoryLocalServiceTracker(BundleContext bundleContext){
        super(bundleContext, CategoryLocalService.class, null);
    }
}
