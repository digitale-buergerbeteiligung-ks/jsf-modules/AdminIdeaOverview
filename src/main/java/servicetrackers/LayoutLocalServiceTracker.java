package servicetrackers;

import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;
import com.liferay.portal.kernel.service.LayoutLocalService;

public class LayoutLocalServiceTracker extends ServiceTracker<LayoutLocalService,LayoutLocalService> {
    public LayoutLocalServiceTracker(BundleContext bundleContext){
        super(bundleContext, LayoutLocalService.class, null);
    }
}
