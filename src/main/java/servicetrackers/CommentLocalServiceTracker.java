package servicetrackers;

import org.osgi.framework.BundleContext;
import org.osgi.util.tracker.ServiceTracker;
import ideaService.service.CommentLocalService;

public class CommentLocalServiceTracker extends ServiceTracker<CommentLocalService, CommentLocalService> {

    public CommentLocalServiceTracker(BundleContext bundleContext){
        super(bundleContext, CommentLocalService.class, null);
    }
}